package com.sud.mylibrary;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.List;

public class Mainclass {
    private static Mainclass mainclass;

    public static Mainclass getInstance() {
        if (mainclass == null) {
            mainclass = new Mainclass();
        }
        return mainclass;
    }

    public List<Data> loadApps(Context context) throws PackageManager.NameNotFoundException {
        PackageManager manager;
        List<Data> apps;
        manager = context.getPackageManager();
        apps = new ArrayList<Data>();
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {
            Data app = new Data();
            app.label = ri.loadLabel(manager);
            app.name = ri.activityInfo.packageName;
            app.icon = ri.activityInfo.loadIcon(manager);
            app.versionCode = manager.getPackageInfo(ri.activityInfo.packageName,0).versionCode;
            app.versionName = manager.getPackageInfo(ri.activityInfo.packageName,0).versionName;
            apps.add(app);
        }
        return apps;
    }
}
