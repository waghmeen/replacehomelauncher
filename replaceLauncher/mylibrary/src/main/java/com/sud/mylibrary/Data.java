package com.sud.mylibrary;
import android.graphics.drawable.Drawable;
import java.lang.CharSequence;



public class Data {
   public CharSequence label;
   public String name;
   public Drawable icon;
   public CharSequence versionName;
   public int versionCode;
   public CharSequence launcherActivity;

}
