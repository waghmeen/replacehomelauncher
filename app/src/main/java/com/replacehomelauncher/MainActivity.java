package com.replacehomelauncher;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.sud.mylibrary.Data;
import com.sud.mylibrary.Mainclass;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerViewAdapterSearch rcAdapter;
    private RecyclerView recyclerView;
    private FloatingSearchView floatingSearchView;
    private List<Data> installApplications;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_list);
        try {
            findView();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void findView() throws PackageManager.NameNotFoundException {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        floatingSearchView = findViewById(R.id.floating_search_view);
        floatingSearchView.setDimBackground(false);
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            public void onSearchTextChanged(String str, String str2) {
                if (!str2.isEmpty()) {
                    rcAdapter.getFilter().filter(str2);
                    return;
                }
                //installApplications = new ArrayList(dummyList);
                rcAdapter = new RecyclerViewAdapterSearch(installApplications);
                recyclerView.setAdapter(rcAdapter);
            }
        });
        getAppList();
    }

    private void getAppList() throws PackageManager.NameNotFoundException {
        installApplications = Mainclass.getInstance().loadApps(getApplicationContext());
        rcAdapter = new RecyclerViewAdapterSearch(installApplications);
        recyclerView.setAdapter(rcAdapter);
    }

    class RecyclerViewAdapterSearch extends RecyclerView.Adapter<MyViewHolderSearch> implements Filterable {
        List<Data> appList;
        List<Data> appListAll = new ArrayList();

        public RecyclerViewAdapterSearch(List<Data> list) {
            this.appList = list;
            this.appListAll.addAll(list);
        }

        public MyViewHolderSearch onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
            final MyViewHolderSearch myViewHolderSearch = new MyViewHolderSearch(inflate);
            inflate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage(String.valueOf(appList.get(myViewHolderSearch.getAdapterPosition()).name));
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    }
                }
            });
            return myViewHolderSearch;
        }

        public void onBindViewHolder(MyViewHolderSearch myViewHolderSearch, int i) {
            myViewHolderSearch.app_name.setText(appList.get(i).name);
            myViewHolderSearch.lable_app.setText(appList.get(i).label);
            myViewHolderSearch.launcherIcone.setImageDrawable(appList.get(i).icon);
            myViewHolderSearch.versionName.setText(appList.get(i).versionName);
            myViewHolderSearch.versionCode.setText(String.valueOf(appList.get(i).versionCode));

        }

        public int getItemCount() {
            return this.appList.size();
        }

        public Filter getFilter() {
            return new Filter() {
                public String convertResultToString(Object obj) {
                    return ((Data) obj).name;
                }

                public FilterResults performFiltering(CharSequence charSequence) {
                    FilterResults filterResults = new FilterResults();
                    ArrayList arrayList = new ArrayList();
                    if (charSequence != null) {
                        String[] split = charSequence.toString().toLowerCase().split(" ");
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("(((((*****");
                        sb.append(RecyclerViewAdapterSearch.this.appListAll.size());
                        printStream.println(sb.toString());
                        for (Data cpoAppointmentDetailsDTO : RecyclerViewAdapterSearch.this.appListAll) {
                            if (cpoAppointmentDetailsDTO.name.toLowerCase().replaceAll("\\s+", "").contains(split[split.length - 1])) {
                                arrayList.add(cpoAppointmentDetailsDTO);
                            }
                        }
                        filterResults.values = arrayList;
                        filterResults.count = arrayList.size();
                    }
                    return filterResults;
                }


                public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    RecyclerViewAdapterSearch.this.appList.clear();
                    for (Object next : (List) filterResults.values) {
                        if (next instanceof Data) {
                            RecyclerViewAdapterSearch.this.appList.add((Data) next);
                        }
                    }
                    RecyclerViewAdapterSearch.this.notifyDataSetChanged();
                }
            };
        }
    }

    public class MyViewHolderSearch extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView launcherIcone = this.itemView.findViewById(R.id.launcherIcone);
        TextView lable_app = this.itemView.findViewById(R.id.lable_app);
        TextView app_name = this.itemView.findViewById(R.id.app_name);
        TextView versionName = this.itemView.findViewById(R.id.versionName);
        TextView versionCode = this.itemView.findViewById(R.id.versionCode);


        public void onClick(View view) {
        }

        public MyViewHolderSearch(View view) {
            super(view);
        }
    }

}
